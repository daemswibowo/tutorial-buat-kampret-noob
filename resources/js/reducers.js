import counter from './reducers/counter';
import { combineReducers } from 'redux';
import user from './reducers/user';

export default combineReducers({
  counter,
  user,
})