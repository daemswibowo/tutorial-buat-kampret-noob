const init = {
  loading: false,
  user: {},
  authorization: null,
  isAuthenticated: false,
  error: null,
}

export default (state=init, action) => {
  const { error, payload, type } = action;
  let additional = {};
  const parsedAuthorization = JSON.parse(localStorage.getItem('AUTHORIZATION'));
  if (parsedAuthorization) {
    additional = {
      authorization: parsedAuthorization,
      isAuthenticated: true,
    }
  }

  switch (type) {
      case 'GET_OUT':
        localStorage.clear();
        return {
          ...state,
          authorization: null,
          isAuthenticated: false,
        }
      case "GET_TOKEN":
          return {
              ...state,
              loading: true
          };
      case "GET_TOKEN_FAIL":
      case "GET_TOKEN_SUCCESS":
          if (!error && payload && payload.data) {
            localStorage.setItem('AUTHORIZATION', JSON.stringify(payload.data));
          }
          return {
              ...state,
              loading: false,
              error: error ? error : null,
              authorization: !error ? payload.data : null,
              isAuthenticated: !error ? true : false,
          };
      default:
          return {
              ...state,
              ...additional,
          };
  }
}