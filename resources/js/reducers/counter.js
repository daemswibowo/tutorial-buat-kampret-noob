const init = {
  count: 100,
};

export default (state=init, action) => {
  switch (action.type) {
    case 'UBAH_COUNT':
      return {
        ...state,
        count: action.nilaiBaru,
      }
  
    default:
      return state;
  }
}