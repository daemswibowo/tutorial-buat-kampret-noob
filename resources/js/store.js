import { createStore, applyMiddleware } from "redux";
import reducers from "./reducers";
import Axios from "axios";
import { API_URL } from "./config";
import { multiClientMiddleware } from "redux-axios-middleware";
import clients from "./clients";


export default createStore(reducers, applyMiddleware(multiClientMiddleware(clients)));