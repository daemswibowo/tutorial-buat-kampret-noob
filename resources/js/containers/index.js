import React from "react";
import { Provider } from "react-redux";
import store from "../store";
import AppContainer from "./AppContainer";

const Containers = () => {
    return (
        <Provider store={store}>
            <AppContainer />
        </Provider>
    );
};

export default Containers;
