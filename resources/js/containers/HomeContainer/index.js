import React, { useState } from 'react'
import { Container, Row, Col, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ubahCount } from '../../actions/counter';

const HomeContainer = ({ count, ubahCount }) => {

  const tambah = () => {
    ubahCount(count + 1);
  }

  const kurang = () => {
    ubahCount(count - 1);
  }

  return (
    <Container>
      <Row>
        <Col>
          <h1>Simple counter app adflkhdf</h1>
          <h2>{count}</h2>
          <Button onClick={() => kurang()}>-</Button>
          <Button onClick={() => tambah()}>+</Button>
        </Col>
      </Row>
    </Container>
  )
}

const mapStateToProps = (state) => {
  return {
    ...state.counter,
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ ubahCount }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
