import React from 'react'
import { Container, Row, Col, Card, Form, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getToken } from '../../actions/user';
import { useForm } from 'react-hook-form';

const LoginContainer = ({ getToken, loading }) => {
  const { register, handleSubmit } = useForm();
  return (
      <Container className="h-100">
          <Row className="h-100 d-flex justify-content-center align-items-center">
              <Col md={4}>
                  <Card>
                      <Card.Body>
                          <Form onSubmit={handleSubmit(getToken)}>
                              <Form.Group>
                                  <Form.Label>Username</Form.Label>
                                  <Form.Control
                                      disabled={loading}
                                      ref={register}
                                      name="username"
                                      type="text"
                                  />
                              </Form.Group>
                              <Form.Group>
                                  <Form.Label>Password</Form.Label>
                                  <Form.Control
                                      disabled={loading}
                                      ref={register}
                                      name="password"
                                      type="password"
                                  />
                              </Form.Group>
                              <Button
                                  type="submit"
                                  disabled={loading}
                                  block
                                  variant="primary"
                              >
                                  {loading ? "Loading..." : "Login"}
                              </Button>
                          </Form>
                      </Card.Body>
                  </Card>
              </Col>
          </Row>
      </Container>
  );
}

const mapStateToProps = ({user}) => ({
  ...user,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getToken}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
