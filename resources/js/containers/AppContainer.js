import React from 'react'
import { BrowserRouter, Switch, Route, Link, Redirect } from "react-router-dom";
import HomeContainer from "./HomeContainer";
import { Navbar, Nav } from "react-bootstrap";
import ProfileContainer from "./ProfileContainer";
import LoginContainer from "./LoginContainer";
import { connect } from 'react-redux';
import { getOut } from '../actions/user';
import { bindActionCreators } from 'redux';
import PrivateRoute from '../components/PrivateRoute';

const AppContainer = ({ isAuthenticated, getOut }) => {
  return (
      <BrowserRouter>
          {isAuthenticated && (
              <Navbar bg="light" expand="lg">
                  <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
                  <Navbar.Toggle aria-controls="basic-navbar-nav" />
                  <Navbar.Collapse id="basic-navbar-nav">
                      <Nav className="mr-auto">
                          <Nav.Link as={Link} to="/">
                              Home
                          </Nav.Link>
                          <Nav.Link as={Link} to="/profile">
                              Profile
                          </Nav.Link>
                      </Nav>
                  </Navbar.Collapse>
                  <Nav className="mr-auto">
                      <Nav.Link href="#" onClick={() => getOut()}>
                          Logout
                      </Nav.Link>
                  </Nav>
              </Navbar>
          )}
          <Switch>
              <PrivateRoute exact path="/">
                  <HomeContainer />
              </PrivateRoute>
              <PrivateRoute path="/profile">
                  <ProfileContainer />
              </PrivateRoute>
              <Route
                  path="/login"
                  render={() =>
                      isAuthenticated ? <Redirect to="/" /> : <LoginContainer />
                  }
              />
          </Switch>
      </BrowserRouter>
  );
}

const mapStateToProps = ({user: { isAuthenticated }}) => ({
  isAuthenticated,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getOut}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
