import Axios from "axios";
import { API_URL } from "./config";

export default {
    default: {
        client: Axios.create({
            baseURL: API_URL,
            responseType: "json"
        }),
        options: {}
    },
    api: {
        client: Axios.create({
            baseURL: `${API_URL}/api`,
            responseType: "json"
        }),
        options: {
            interceptors: {
                request: [
                    ({ getState, dispatch, getSourceAction }, req) => {
                        // if (getState().user.token) {
                        //   req.headers["Authorization"] = getState().user.token;
                        // }
                        return req;
                    }
                ]
            }
        }
    }
};
