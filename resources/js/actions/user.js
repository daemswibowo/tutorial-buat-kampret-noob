import { CLIENT_ID, CLIENT_SECRET } from "../config";

export const getToken = data => ({
    type: "GET_TOKEN",
    payload: {
        request: {
            url: "/oauth/token",
            method: "post",
            data: {
                ...data,
                client_id: CLIENT_ID,
                client_secret: CLIENT_SECRET,
                grant_type: "password"
            },
        },
    },
});

export const getOut = () => ({
    type: 'GET_OUT',
});

export default {
    getToken,
    getOut,
}
