<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::updateOrCreate([
            'name' => 'Candra Ganteng',
            'email' => 'email@foo.com',
            'password' => bcrypt('abc1234'),
        ]);
    }
}
